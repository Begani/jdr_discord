class Player {
    /**
     * 
     * @param {String} name
     * @param {String} race 
     * @param {String} classe 
     * @param {Number} age 
     */

    constructor(name, race, classe, age, force, dexterite, intelligence, sagesse, charisme , constitution) {
        this.name = name;
        this.race = race;
        this.classe = classe;
        this.age = age;
        this.niveau = 1;
        this.quetes = 0;
        this.pv = 10;
        this.force = 8 + force;
        this.dexterite = 8 + dexterite;
        this.intelligence = 8 + intelligence;
        this.sagesse = 8 + sagesse;
        this.charisme = 8 + charisme;
        this.constitution = 8 + constitution;
    }


    message(message) {
        message.channel.send(`
        ${message.author.username} rejoint la partie !\r
        Nom : ${this.name}\r
        Race : ${this.race}\r
        Classe : ${this.classe}\r
        Age : ${this.age}\r
        Niveau : ${this.niveau}\r
        Quêtes réalisées : ${this.quetes}\r
        Pv: ${this.pv}\r
        Force: ${this.force}\r
        Dexterite: ${this.dexterite}\r
        Intelligence: ${this.intelligence}\r
        Sagesse: ${this.sagesse}\r
        Charisme: ${this.charisme}\r
        Constitution: ${this.constitution}\r
        `);
    }

}

module.exports = Player;
