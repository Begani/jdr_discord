class Mob {
    /**
     * This file is used to create a monster when event Fight is triggered
     * @param {String} name 
     * @param {Number} pv 
     * @param {Number} lvl 
     */
    //Only MJ can interacts with this file
    constructor(name, pv, lvl) {

        this.aOfMob = [];
        this.name = name;
        this.pv = pv;
        this.lvl = lvl;
        this.initializeArray();
        this.create();
    }
    create() {
        //Get mob in Array of mobs
        let mob = aOfMob[this.name];

        //Set stats from MJ
        mob.pv = this.pv;
        mob.lvl = this.lvl;
        console.log(mob);
    }

    initializeArray() {
        this.aOfMob["Zorg"] = { nom: "Zorg", pv: 0 };

        this.aOfMob["CouilleM"] = { nom: "Couille Molle", pv: 10 };
        this.aOfMob["CouilleV"] = { nom: "Couille Volante", pv: 10 };
        this.aOfMob["Loup"] = { nom: "Le Méchant Vilain Loup", pv: 10 };
    }
}