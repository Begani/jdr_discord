const De = require("./src/de.js");
const Player = require("./src/player.js");
const Combat = require("./src/combat.js");
const Discord = require('discord.js');

const { prefix, token } = require('./config.json');

const client = new Discord.Client();

/**
 * @Array
 * Tableau des joueurs
 */
let aOfPlayers = [];

/**
 * @let
 * Nombre de joueur dans la partie
 */
let nbJoueur = 0;

/**
 * @let
 * Message récupéré dans le channel
 */
let sInstruction = "";

/**
* @let 
* Nom du joueur
*/
let player = "";

client.once('ready', async () => {
    console.log('Début du programme !');
});


/**
 * Ce bot écoute tous les messages
 */
client.on('message', (message) => {
    if (message.author.username == "SouperCalcoulator") {
        console.log("ecoute pas idiot");
    }
    else {
        sInstruction = message.content;

        console.log('Liste des joueurs et stats :' + aOfPlayers);

        let aOfPlayerInfo = sInstruction.replace(' ', ',');
        aOfPlayerInfo = aOfPlayerInfo.split(',');
        //Delete spaces for all values
        for (y = 0; y < aOfPlayerInfo.length; y++) {
            aOfPlayerInfo[y] = aOfPlayerInfo[y].trim()
        }
        let cmd = aOfPlayerInfo[0];
        //Cmd : rejoindre la partie -> !play NomRandom, Elfe des montagnes, Mage de feu, 1118 ans
        if (cmd == `${prefix}play`) {
            player = message.author.username;

            if (sInstruction == `${prefix}play info`) {
                message.channel.send(`Format d'inscription :\r!play nom, race, classe, âge\rA toi de jouer ;)`);
            }
            else if (sInstruction == `${prefix}play`) {
                message.channel.send(`Entrez !play info pour voir comment participer à une partie `);
            }
            console.log("Nom du joueur : " + player)
            console.log(" Le joueur existe ?" + aOfPlayers.indexOf(player))
            //Vérifie si le joueur est déjà présent dans la liste de joueurs
            if (nbJoueur > 0 && aOfPlayers != "undefined") {
                message.channel.send(`@${message.author.tag} est déjà là !`);
                console.log('Erreur, le joueur est déjà là !');
            }
            else {

                //Augmente le nombre de joueur
                nbJoueur++;

                let avatarName = aOfPlayerInfo[1];
                let avatarRace = aOfPlayerInfo[2];
                let avatarClasse = aOfPlayerInfo[3];
                let avatarAge = aOfPlayerInfo[4];
                let force = parseInt(aOfPlayerInfo[5]);
                let dexterite = parseInt(aOfPlayerInfo[6]);
                let intelligence = parseInt(aOfPlayerInfo[7]);
                let sagesse = parseInt(aOfPlayerInfo[8]);
                let charisme = parseInt(aOfPlayerInfo[9]);
                let constitution = parseInt(aOfPlayerInfo[10]);
                let totalStat = force + dexterite + intelligence + sagesse + charisme + constitution;


                if (totalStat == 12) {
                    //Création du joueur 
                    let newPlayer = new Player(avatarName, avatarRace, avatarClasse, avatarAge, force, dexterite, intelligence, sagesse, charisme, constitution);
                    //Create player in programm and set his stats
                    console.log("Nom du joueur : " + player);
                    aOfPlayers[player] = {
                        name: newPlayer.name,
                        race: newPlayer.race,
                        classe: newPlayer.classe,
                        age: newPlayer.age,
                        niveau: newPlayer.niveau,
                        quetes: newPlayer.quetes,
                        pv: newPlayer.pv,
                        force: newPlayer.force,
                        dexterite: newPlayer.dexterite,
                        intelligence: newPlayer.intelligence,
                        sagesse: newPlayer.sagesse,
                        charisme: newPlayer.charisme,
                        constitution: newPlayer.constitution
                    };
                    console.log(aOfPlayers);
                    newPlayer.message(message);
                }
                else if (totalStat < 12) {
                    message.channel.send(`${player} il te manque ${12 - totalStat} stats à attribuer à ton personnage`);
                }
                else if (totalStat > 12) {
                    message.channel.send(`${player} t'es un tricheur tu as voulu attribuer ${totalStat} statsà ton personnage, il en faut 12 au total`);
                }
            }

        }
        //Cmd : Lancer la partie
        else if (sInstruction === `${prefix}pg`) {
            if (aOfPlayers.length < 4) {
                let diff = 4 - aOfPlayers.length;
                message.channel.send(`Il manque ${diff} joueurs`);
            }
            else {
                /**
                 * Coming Soon
                 */
            }
        }
        //lancer le combat : !combat Nom du joueur, nom Du Mob
        else if (sInstruction.includes(`${prefix}combat`) == true) {
            combat = new Combat(player, nomdumob);

            console.log(mob);
            //Annoncement du combat
            message.channel.send(`Le combat entre  ${contender}(${aOfPlayers[nbJoueur]['pv']}) et  le terrible ${mob.nom}(${mob.pv}) commence !`);
            //Initiative
            message.channel.send(`${mob.nom} lance les dés : ${mobIni} !`);
            message.channel.send(`${contender} lance les dés : ${playerIni} !`);
            //Vainqueur Initiative
            message.channel.send(`${winnerIni} remporte l'initiative`);
        }
        /**
         * Lancé de dé :
         * Cmd : !de (6,20)
         */
        else if (sInstruction.includes(`${prefix}de`) == true) {
            if (sInstruction.length > 4) {
                faces = sInstruction.substr(4);
            }
            else {
                faces = "none";
            }
            console.log(faces);
            let de = new De(faces);
            de.chooseDice(message);
        }
    }

});


client.login(token);






